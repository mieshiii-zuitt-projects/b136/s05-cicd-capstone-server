const express = require("express");

const app = express();
const port = process.env.PORT || 8080;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/", (req, res) => {
	res.send('Hello World');
});

app.post("/results", (req, res) => {
    res.send(req.body);
});
app.listen(port, () => console.log(`Server running at port ${port}`));